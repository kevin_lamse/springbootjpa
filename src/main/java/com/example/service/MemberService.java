package com.example.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.repository.MemberOrgRepository;
import com.example.model.*;

@Service
public class MemberService {

	// needs db connection
	//needs to return entity
	@Autowired
	MemberOrgRepository memberOrgRepository;
	
	public Iterable<MemberOrg> getMemberList(){
		return memberOrgRepository.findAll();
	}
	
	public List<MemberOrg> findFirst10ByOrgId(Long orgId) {
		return memberOrgRepository.findFirst10ByOrgId(orgId);
	}
}
