package com.example.repository;

import java.util.List;

import org.springframework.data.domain.Sort;
import org.springframework.data.repository.CrudRepository;

import com.example.model.*;

//Long vs long
public interface MemberOrgRepository extends CrudRepository<MemberOrg, Long> {

	//Note:  this is designed to search with multiple matches...not primary keys.
	List<MemberOrg> findFirst10ByOrgId(Long orgId );
}
