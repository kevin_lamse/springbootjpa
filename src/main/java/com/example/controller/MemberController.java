package com.example.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.example.model.MemberOrg;
import com.example.service.MemberService;

@Controller
public class MemberController {

	@Autowired
	MemberService memberService;
	
	@RequestMapping("/members")
	public String showMembers(Model model) {
		
		Iterable<MemberOrg> members = memberService.getMemberList();
		
		model.addAttribute("members", members);
		return "members/list";
	}
	

	
}
