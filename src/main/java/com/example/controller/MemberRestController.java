package com.example.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.model.MemberOrg;
import com.example.service.MemberService;

@RestController
public class MemberRestController {

	@Autowired
	MemberService memberService;
	@RequestMapping("/api/members")
	public Iterable<MemberOrg> showMembers(Model model) {
		
		Long l = new Long(3);
		Iterable<MemberOrg> members = memberService.findFirst10ByOrgId(l);
		return members;
	}

}
