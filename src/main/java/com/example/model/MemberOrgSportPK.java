package com.example.model;

import java.io.Serializable;
import javax.persistence.*;

/**
 * The primary key class for the MEMBER_ORG_SPORT database table.
 * 
 */
@Embeddable
public class MemberOrgSportPK implements Serializable {
	//default serial version id, required for serializable classes.
	private static final long serialVersionUID = 1L;

	@Column(name="ORG_ID", insertable=false, updatable=false)
	private long orgId;

	@Column(name="ACADEMIC_YEAR")
	private long academicYear;

	@Column(name="SPORT_CODE", insertable=false, updatable=false)
	private String sportCode;

	public MemberOrgSportPK() {
	}
	public long getOrgId() {
		return this.orgId;
	}
	public void setOrgId(long orgId) {
		this.orgId = orgId;
	}
	public long getAcademicYear() {
		return this.academicYear;
	}
	public void setAcademicYear(long academicYear) {
		this.academicYear = academicYear;
	}
	public String getSportCode() {
		return this.sportCode;
	}
	public void setSportCode(String sportCode) {
		this.sportCode = sportCode;
	}

	public boolean equals(Object other) {
		if (this == other) {
			return true;
		}
		if (!(other instanceof MemberOrgSportPK)) {
			return false;
		}
		MemberOrgSportPK castOther = (MemberOrgSportPK)other;
		return 
			(this.orgId == castOther.orgId)
			&& (this.academicYear == castOther.academicYear)
			&& this.sportCode.equals(castOther.sportCode);
	}

	public int hashCode() {
		final int prime = 31;
		int hash = 17;
		hash = hash * prime + ((int) (this.orgId ^ (this.orgId >>> 32)));
		hash = hash * prime + ((int) (this.academicYear ^ (this.academicYear >>> 32)));
		hash = hash * prime + this.sportCode.hashCode();
		
		return hash;
	}
}