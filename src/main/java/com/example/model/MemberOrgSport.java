package com.example.model;

import java.io.Serializable;
import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonBackReference;

import java.math.BigDecimal;


/**
 * The persistent class for the MEMBER_ORG_SPORT database table.
 * 
 */
@Entity
@Table(name="MEMBER_ORG_SPORT")
@NamedQuery(name="MemberOrgSport.findAll", query="SELECT m FROM MemberOrgSport m")
public class MemberOrgSport implements Serializable {
	private static final long serialVersionUID = 1L;

	@EmbeddedId
	private MemberOrgSportPK id;

	private BigDecimal conference;

	@Column(name="CONTESTS_ACTUAL")
	private BigDecimal contestsActual;

	@Column(name="CONTESTS_PROJECTED")
	private BigDecimal contestsProjected;

	@Column(name="DISAGREE_PREV_YEAR")
	private String disagreePrevYear;

	private BigDecimal division;

	@Column(name="GRANTS_IN_AID_COUNT")
	private BigDecimal grantsInAidCount;

	@Column(name="GRANTS_OTHER_COUNT")
	private BigDecimal grantsOtherCount;

	@Column(name="HOLLOW_SPORT_FLAG")
	private String hollowSportFlag;

	@Column(name="ISS_FLAG")
	private String issFlag;

	@Column(name="OWNER_ID")
	private String ownerId;

	@Column(name="PARTICIPANT_COUNT")
	private BigDecimal participantCount;

	@Column(name="POST_SEASON_CHAMPION")
	private String postSeasonChampion;

	@Column(name="REASON_NOT_SPONSOR_NEXTYR")
	private String reasonNotSponsorNextyr;

	@Column(name="REASON_NOT_SPONSORING")
	private String reasonNotSponsoring;

	@Column(name="RECLASS_DIVISION")
	private BigDecimal reclassDivision;

	@Column(name="RECLASS_START_YEAR")
	private BigDecimal reclassStartYear;

	@Column(name="RECLASS_SUBDIVISION")
	private BigDecimal reclassSubdivision;

	@Column(name="RECLASS_YEAR")
	private BigDecimal reclassYear;

	@Column(name="REV_DIST_REQ")
	private String revDistReq;

	@Column(name="SEC_CONFERENCE")
	private BigDecimal secConference;

	@Column(name="SPORT_REGION")
	private BigDecimal sportRegion;

	@Column(name="SQUAD_SIZE")
	private BigDecimal squadSize;

	private BigDecimal subdivision;

	//bi-directional many-to-one association to MemberOrg
	@ManyToOne
	@JoinColumn(name="ORG_ID", insertable=false, updatable=false)
	@JsonBackReference
	private MemberOrg memberOrg;

	public MemberOrgSport() {
	}

	public MemberOrgSportPK getId() {
		return this.id;
	}

	public void setId(MemberOrgSportPK id) {
		this.id = id;
	}

	public BigDecimal getConference() {
		return this.conference;
	}

	public void setConference(BigDecimal conference) {
		this.conference = conference;
	}

	public BigDecimal getContestsActual() {
		return this.contestsActual;
	}

	public void setContestsActual(BigDecimal contestsActual) {
		this.contestsActual = contestsActual;
	}

	public BigDecimal getContestsProjected() {
		return this.contestsProjected;
	}

	public void setContestsProjected(BigDecimal contestsProjected) {
		this.contestsProjected = contestsProjected;
	}

	public String getDisagreePrevYear() {
		return this.disagreePrevYear;
	}

	public void setDisagreePrevYear(String disagreePrevYear) {
		this.disagreePrevYear = disagreePrevYear;
	}

	public BigDecimal getDivision() {
		return this.division;
	}

	public void setDivision(BigDecimal division) {
		this.division = division;
	}

	public BigDecimal getGrantsInAidCount() {
		return this.grantsInAidCount;
	}

	public void setGrantsInAidCount(BigDecimal grantsInAidCount) {
		this.grantsInAidCount = grantsInAidCount;
	}

	public BigDecimal getGrantsOtherCount() {
		return this.grantsOtherCount;
	}

	public void setGrantsOtherCount(BigDecimal grantsOtherCount) {
		this.grantsOtherCount = grantsOtherCount;
	}

	public String getHollowSportFlag() {
		return this.hollowSportFlag;
	}

	public void setHollowSportFlag(String hollowSportFlag) {
		this.hollowSportFlag = hollowSportFlag;
	}

	public String getIssFlag() {
		return this.issFlag;
	}

	public void setIssFlag(String issFlag) {
		this.issFlag = issFlag;
	}

	public String getOwnerId() {
		return this.ownerId;
	}

	public void setOwnerId(String ownerId) {
		this.ownerId = ownerId;
	}

	public BigDecimal getParticipantCount() {
		return this.participantCount;
	}

	public void setParticipantCount(BigDecimal participantCount) {
		this.participantCount = participantCount;
	}

	public String getPostSeasonChampion() {
		return this.postSeasonChampion;
	}

	public void setPostSeasonChampion(String postSeasonChampion) {
		this.postSeasonChampion = postSeasonChampion;
	}

	public String getReasonNotSponsorNextyr() {
		return this.reasonNotSponsorNextyr;
	}

	public void setReasonNotSponsorNextyr(String reasonNotSponsorNextyr) {
		this.reasonNotSponsorNextyr = reasonNotSponsorNextyr;
	}

	public String getReasonNotSponsoring() {
		return this.reasonNotSponsoring;
	}

	public void setReasonNotSponsoring(String reasonNotSponsoring) {
		this.reasonNotSponsoring = reasonNotSponsoring;
	}

	public BigDecimal getReclassDivision() {
		return this.reclassDivision;
	}

	public void setReclassDivision(BigDecimal reclassDivision) {
		this.reclassDivision = reclassDivision;
	}

	public BigDecimal getReclassStartYear() {
		return this.reclassStartYear;
	}

	public void setReclassStartYear(BigDecimal reclassStartYear) {
		this.reclassStartYear = reclassStartYear;
	}

	public BigDecimal getReclassSubdivision() {
		return this.reclassSubdivision;
	}

	public void setReclassSubdivision(BigDecimal reclassSubdivision) {
		this.reclassSubdivision = reclassSubdivision;
	}

	public BigDecimal getReclassYear() {
		return this.reclassYear;
	}

	public void setReclassYear(BigDecimal reclassYear) {
		this.reclassYear = reclassYear;
	}

	public String getRevDistReq() {
		return this.revDistReq;
	}

	public void setRevDistReq(String revDistReq) {
		this.revDistReq = revDistReq;
	}

	public BigDecimal getSecConference() {
		return this.secConference;
	}

	public void setSecConference(BigDecimal secConference) {
		this.secConference = secConference;
	}

	public BigDecimal getSportRegion() {
		return this.sportRegion;
	}

	public void setSportRegion(BigDecimal sportRegion) {
		this.sportRegion = sportRegion;
	}

	public BigDecimal getSquadSize() {
		return this.squadSize;
	}

	public void setSquadSize(BigDecimal squadSize) {
		this.squadSize = squadSize;
	}

	public BigDecimal getSubdivision() {
		return this.subdivision;
	}

	public void setSubdivision(BigDecimal subdivision) {
		this.subdivision = subdivision;
	}

	public MemberOrg getMemberOrg() {
		return this.memberOrg;
	}

	public void setMemberOrg(MemberOrg memberOrg) {
		this.memberOrg = memberOrg;
	}

	@Override
	public String toString() {
		return "MemberOrgSport [id=" + id + ", conference=" + conference + ", contestsActual=" + contestsActual
				+ ", contestsProjected=" + contestsProjected + ", disagreePrevYear=" + disagreePrevYear + ", division="
				+ division + ", grantsInAidCount=" + grantsInAidCount + ", grantsOtherCount=" + grantsOtherCount
				+ ", hollowSportFlag=" + hollowSportFlag + ", issFlag=" + issFlag + ", ownerId=" + ownerId
				+ ", participantCount=" + participantCount + ", postSeasonChampion=" + postSeasonChampion
				+ ", reasonNotSponsorNextyr=" + reasonNotSponsorNextyr + ", reasonNotSponsoring=" + reasonNotSponsoring
				+ ", reclassDivision=" + reclassDivision + ", reclassStartYear=" + reclassStartYear
				+ ", reclassSubdivision=" + reclassSubdivision + ", reclassYear=" + reclassYear + ", revDistReq="
				+ revDistReq + ", secConference=" + secConference + ", sportRegion=" + sportRegion + ", squadSize="
				+ squadSize + ", subdivision=" + subdivision + ", memberOrg=" + memberOrg + "]";
	}

}