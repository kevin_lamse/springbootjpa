package com.example.model;

import java.io.Serializable;
import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonManagedReference;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;


/**
 * The persistent class for the MEMBER_ORG database table.
 * 
 */
@Entity
@Table(name="MEMBER_ORG")
@NamedQuery(name="MemberOrg.findAll", query="SELECT m FROM MemberOrg m")
public class MemberOrg implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="ORG_ID")
	private long orgId;

	@Column(name="ATHLETE_WEB_URL")
	private String athleteWebUrl;

	@Temporal(TemporalType.DATE)
	@Column(name="DATE_MEMBERSHIP")
	private Date dateMembership;

	@Column(name="DELETE_FLAG")
	private String deleteFlag;

	@Column(name="EXPLORATORY_FLAG")
	private String exploratoryFlag;

	@Temporal(TemporalType.DATE)
	@Column(name="EXPLORATORY_START_DATE")
	private Date exploratoryStartDate;

	@Column(name="FAX_NUMBER")
	private String faxNumber;

	@Column(name="FEDERAL_ID_NUM")
	private BigDecimal federalIdNum;

	@Column(name="GEO_REGION")
	private BigDecimal geoRegion;

	@Column(name="HISTORICALLY_BLACK_FLAG")
	private String historicallyBlackFlag;

	@Column(name="ISSG_ACCREDITING_AGENCY")
	private String issgAccreditingAgency;

	@Temporal(TemporalType.DATE)
	@Column(name="ISSG_LAST_ACCREDITATION_DATE")
	private Date issgLastAccreditationDate;

	@Temporal(TemporalType.DATE)
	@Column(name="ISSG_NEXT_REVIEW_DATE")
	private Date issgNextReviewDate;

	@Temporal(TemporalType.DATE)
	@Column(name="ISSG_START_DATE")
	private Date issgStartDate;

	@Column(name="ISSG_STATUS_FLAG")
	private String issgStatusFlag;

	@Column(name="NAIA_CODE")
	private String naiaCode;

	@Column(name="NAME_MAIL")
	private String nameMail;

	@Column(name="NAME_OFFICIAL")
	private String nameOfficial;

	@Column(name="NAME_SIGNAGE")
	private String nameSignage;

	@Column(name="NAME_SORTED")
	private String nameSorted;

	@Column(name="NAME_STORY")
	private String nameStory;

	@Column(name="NAME_TABULAR")
	private String nameTabular;

	@Column(name="NAME_TABULAR_MEN")
	private String nameTabularMen;

	@Column(name="NAME_TABULAR_WOMEN")
	private String nameTabularWomen;

	@Column(name="NCCAA_FLAG")
	private String nccaaFlag;

	@Column(name="OWNER_ID")
	private String ownerId;

	@Column(name="PHONE_NUMBER")
	private String phoneNumber;

	@Column(name="PRIVATE_FLAG")
	private String privateFlag;

	@Column(name="PROVISIONAL_START_YEAR")
	private BigDecimal provisionalStartYear;

	@Column(name="PROVISIONAL_YEAR")
	private BigDecimal provisionalYear;

	@Column(name="RECLASS_DIVISION")
	private BigDecimal reclassDivision;

	@Column(name="RECLASS_START_YEAR")
	private BigDecimal reclassStartYear;

	@Column(name="RECLASS_SUBDIVISION")
	private BigDecimal reclassSubdivision;

	@Column(name="RECLASS_YEAR")
	private BigDecimal reclassYear;

	@Column(name="TYPE_ID")
	private String typeId;

	@Column(name="VOTING_FLAG")
	private String votingFlag;

	@Column(name="WEB_SITE_URL")
	private String webSiteUrl;

	//bi-directional many-to-one association to MemberOrgSport
	@OneToMany(mappedBy="memberOrg")
	@JsonManagedReference
	private List<MemberOrgSport> memberOrgSports;

	public MemberOrg() {
	}

	public long getOrgId() {
		return this.orgId;
	}

	public void setOrgId(long orgId) {
		this.orgId = orgId;
	}

	public String getAthleteWebUrl() {
		return this.athleteWebUrl;
	}

	public void setAthleteWebUrl(String athleteWebUrl) {
		this.athleteWebUrl = athleteWebUrl;
	}

	public Date getDateMembership() {
		return this.dateMembership;
	}

	public void setDateMembership(Date dateMembership) {
		this.dateMembership = dateMembership;
	}

	public String getDeleteFlag() {
		return this.deleteFlag;
	}

	public void setDeleteFlag(String deleteFlag) {
		this.deleteFlag = deleteFlag;
	}

	public String getExploratoryFlag() {
		return this.exploratoryFlag;
	}

	public void setExploratoryFlag(String exploratoryFlag) {
		this.exploratoryFlag = exploratoryFlag;
	}

	public Date getExploratoryStartDate() {
		return this.exploratoryStartDate;
	}

	public void setExploratoryStartDate(Date exploratoryStartDate) {
		this.exploratoryStartDate = exploratoryStartDate;
	}

	public String getFaxNumber() {
		return this.faxNumber;
	}

	public void setFaxNumber(String faxNumber) {
		this.faxNumber = faxNumber;
	}

	public BigDecimal getFederalIdNum() {
		return this.federalIdNum;
	}

	public void setFederalIdNum(BigDecimal federalIdNum) {
		this.federalIdNum = federalIdNum;
	}

	public BigDecimal getGeoRegion() {
		return this.geoRegion;
	}

	public void setGeoRegion(BigDecimal geoRegion) {
		this.geoRegion = geoRegion;
	}

	public String getHistoricallyBlackFlag() {
		return this.historicallyBlackFlag;
	}

	public void setHistoricallyBlackFlag(String historicallyBlackFlag) {
		this.historicallyBlackFlag = historicallyBlackFlag;
	}

	public String getIssgAccreditingAgency() {
		return this.issgAccreditingAgency;
	}

	public void setIssgAccreditingAgency(String issgAccreditingAgency) {
		this.issgAccreditingAgency = issgAccreditingAgency;
	}

	public Date getIssgLastAccreditationDate() {
		return this.issgLastAccreditationDate;
	}

	public void setIssgLastAccreditationDate(Date issgLastAccreditationDate) {
		this.issgLastAccreditationDate = issgLastAccreditationDate;
	}

	public Date getIssgNextReviewDate() {
		return this.issgNextReviewDate;
	}

	public void setIssgNextReviewDate(Date issgNextReviewDate) {
		this.issgNextReviewDate = issgNextReviewDate;
	}

	public Date getIssgStartDate() {
		return this.issgStartDate;
	}

	public void setIssgStartDate(Date issgStartDate) {
		this.issgStartDate = issgStartDate;
	}

	public String getIssgStatusFlag() {
		return this.issgStatusFlag;
	}

	public void setIssgStatusFlag(String issgStatusFlag) {
		this.issgStatusFlag = issgStatusFlag;
	}

	public String getNaiaCode() {
		return this.naiaCode;
	}

	public void setNaiaCode(String naiaCode) {
		this.naiaCode = naiaCode;
	}

	public String getNameMail() {
		return this.nameMail;
	}

	public void setNameMail(String nameMail) {
		this.nameMail = nameMail;
	}

	public String getNameOfficial() {
		return this.nameOfficial;
	}

	public void setNameOfficial(String nameOfficial) {
		this.nameOfficial = nameOfficial;
	}

	public String getNameSignage() {
		return this.nameSignage;
	}

	public void setNameSignage(String nameSignage) {
		this.nameSignage = nameSignage;
	}

	public String getNameSorted() {
		return this.nameSorted;
	}

	public void setNameSorted(String nameSorted) {
		this.nameSorted = nameSorted;
	}

	public String getNameStory() {
		return this.nameStory;
	}

	public void setNameStory(String nameStory) {
		this.nameStory = nameStory;
	}

	public String getNameTabular() {
		return this.nameTabular;
	}

	public void setNameTabular(String nameTabular) {
		this.nameTabular = nameTabular;
	}

	public String getNameTabularMen() {
		return this.nameTabularMen;
	}

	public void setNameTabularMen(String nameTabularMen) {
		this.nameTabularMen = nameTabularMen;
	}

	public String getNameTabularWomen() {
		return this.nameTabularWomen;
	}

	public void setNameTabularWomen(String nameTabularWomen) {
		this.nameTabularWomen = nameTabularWomen;
	}

	public String getNccaaFlag() {
		return this.nccaaFlag;
	}

	public void setNccaaFlag(String nccaaFlag) {
		this.nccaaFlag = nccaaFlag;
	}

	public String getOwnerId() {
		return this.ownerId;
	}

	public void setOwnerId(String ownerId) {
		this.ownerId = ownerId;
	}

	public String getPhoneNumber() {
		return this.phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public String getPrivateFlag() {
		return this.privateFlag;
	}

	public void setPrivateFlag(String privateFlag) {
		this.privateFlag = privateFlag;
	}

	public BigDecimal getProvisionalStartYear() {
		return this.provisionalStartYear;
	}

	public void setProvisionalStartYear(BigDecimal provisionalStartYear) {
		this.provisionalStartYear = provisionalStartYear;
	}

	public BigDecimal getProvisionalYear() {
		return this.provisionalYear;
	}

	public void setProvisionalYear(BigDecimal provisionalYear) {
		this.provisionalYear = provisionalYear;
	}

	public BigDecimal getReclassDivision() {
		return this.reclassDivision;
	}

	public void setReclassDivision(BigDecimal reclassDivision) {
		this.reclassDivision = reclassDivision;
	}

	public BigDecimal getReclassStartYear() {
		return this.reclassStartYear;
	}

	public void setReclassStartYear(BigDecimal reclassStartYear) {
		this.reclassStartYear = reclassStartYear;
	}

	public BigDecimal getReclassSubdivision() {
		return this.reclassSubdivision;
	}

	public void setReclassSubdivision(BigDecimal reclassSubdivision) {
		this.reclassSubdivision = reclassSubdivision;
	}

	public BigDecimal getReclassYear() {
		return this.reclassYear;
	}

	public void setReclassYear(BigDecimal reclassYear) {
		this.reclassYear = reclassYear;
	}

	public String getTypeId() {
		return this.typeId;
	}

	public void setTypeId(String typeId) {
		this.typeId = typeId;
	}

	public String getVotingFlag() {
		return this.votingFlag;
	}

	public void setVotingFlag(String votingFlag) {
		this.votingFlag = votingFlag;
	}

	public String getWebSiteUrl() {
		return this.webSiteUrl;
	}

	public void setWebSiteUrl(String webSiteUrl) {
		this.webSiteUrl = webSiteUrl;
	}

	public List<MemberOrgSport> getMemberOrgSports() {
		return this.memberOrgSports;
	}

	public void setMemberOrgSports(List<MemberOrgSport> memberOrgSports) {
		this.memberOrgSports = memberOrgSports;
	}

	public MemberOrgSport addMemberOrgSport(MemberOrgSport memberOrgSport) {
		getMemberOrgSports().add(memberOrgSport);
		memberOrgSport.setMemberOrg(this);

		return memberOrgSport;
	}

	public MemberOrgSport removeMemberOrgSport(MemberOrgSport memberOrgSport) {
		getMemberOrgSports().remove(memberOrgSport);
		memberOrgSport.setMemberOrg(null);

		return memberOrgSport;
	}

	@Override
	public String toString() {
		return "MemberOrg [orgId=" + orgId + ", athleteWebUrl=" + athleteWebUrl + ", dateMembership=" + dateMembership
				+ ", deleteFlag=" + deleteFlag + ", exploratoryFlag=" + exploratoryFlag + ", exploratoryStartDate="
				+ exploratoryStartDate + ", faxNumber=" + faxNumber + ", federalIdNum=" + federalIdNum + ", geoRegion="
				+ geoRegion + ", historicallyBlackFlag=" + historicallyBlackFlag + ", issgAccreditingAgency="
				+ issgAccreditingAgency + ", issgLastAccreditationDate=" + issgLastAccreditationDate
				+ ", issgNextReviewDate=" + issgNextReviewDate + ", issgStartDate=" + issgStartDate
				+ ", issgStatusFlag=" + issgStatusFlag + ", naiaCode=" + naiaCode + ", nameMail=" + nameMail
				+ ", nameOfficial=" + nameOfficial + ", nameSignage=" + nameSignage + ", nameSorted=" + nameSorted
				+ ", nameStory=" + nameStory + ", nameTabular=" + nameTabular + ", nameTabularMen=" + nameTabularMen
				+ ", nameTabularWomen=" + nameTabularWomen + ", nccaaFlag=" + nccaaFlag + ", ownerId=" + ownerId
				+ ", phoneNumber=" + phoneNumber + ", privateFlag=" + privateFlag + ", provisionalStartYear="
				+ provisionalStartYear + ", provisionalYear=" + provisionalYear + ", reclassDivision=" + reclassDivision
				+ ", reclassStartYear=" + reclassStartYear + ", reclassSubdivision=" + reclassSubdivision
				+ ", reclassYear=" + reclassYear + ", typeId=" + typeId + ", votingFlag=" + votingFlag + ", webSiteUrl="
				+ webSiteUrl + "]";
	}

}