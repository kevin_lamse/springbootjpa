package com.example;

import javax.annotation.PostConstruct;

import org.hibernate.Hibernate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.transaction.annotation.Transactional;

import com.example.model.MemberOrg;
import com.example.repository.*;

@SpringBootApplication
public class SpringBootJpaApplication {

	private static final Logger logger = LoggerFactory.getLogger("SBJA");
	@Autowired
	MemberOrgRepository memberOrgRepository;
	
	public static void main(String[] args) {
		SpringApplication.run(SpringBootJpaApplication.class, args);
	}
//	
//	@PostConstruct
//	@Transactional
//	void seeMembers() {
//		logger.info(" seeMembers");
//		for(MemberOrg mo : memberOrgRepository.findAll()) {
//			System.out.println(" mo ="+mo);
//			/* does not work
//			if(mo.getOrgId() == 30151) {
//				System.out.println("Print Sport");
//				mo.getMemberOrgSports().size();
//				System.out.println(" mo.sports ="+mo.getMemberOrgSports());
//				
//			}
//			*/
//		}
//	}
}
